package com.demo.springboot.rest;

import com.demo.springboot.model.FunctionZeros;
import com.demo.springboot.service.QuadraticFunction;
import com.demo.springboot.service.QuadraticFunctionImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class DocumentApiController {

    DocumentApiController() {

    }
    //private static final Logger LOGGER = LoggerFactory.getLogger(DocumentApiController.class);

    QuadraticFunction qf = new QuadraticFunctionImpl();

    @GetMapping
    @RequestMapping("/api/math/quadratic-function")
    public ResponseEntity<Map<String, Float>> solve(@RequestParam float a,@RequestParam float b, @RequestParam float c) {
        FunctionZeros functionZeros = qf.solve(a,b,c);
        //LOG.info("--- get all movies: {}", movies.getMovies());
        Map<String, Float> functionAns = new HashMap<>();
        functionAns.put("x1", functionZeros.getX1());
        functionAns.put("x2", functionZeros.getX2());
        return ResponseEntity.ok().body(functionAns);
    }
}
