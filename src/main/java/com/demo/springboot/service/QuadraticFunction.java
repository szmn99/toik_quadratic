package com.demo.springboot.service;

import com.demo.springboot.model.FunctionZeros;

public interface QuadraticFunction {
    FunctionZeros solve(float a, float b, float c);
}
