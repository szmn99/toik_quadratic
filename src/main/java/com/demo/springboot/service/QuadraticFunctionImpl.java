package com.demo.springboot.service;

import com.demo.springboot.model.FunctionZeros;

public class QuadraticFunctionImpl implements QuadraticFunction{
    public FunctionZeros solve(float a, float b, float c) {
        Float x1;
        Float x2;

        float delta = b*b - 4*a*c;
        if(delta<0){
            x1 = null;
            x2 = null;
        }else if(delta == 0){
            x1 = (-b)/2*a;
            x2 = null;
        }else{
            x1 = (float) (((-b) + Math.sqrt(delta)) / 2 * a);
            x2 = (float) (((-b) - Math.sqrt(delta)) / 2 * a);
        }
        return new FunctionZeros(x1,x2);
    }
}
